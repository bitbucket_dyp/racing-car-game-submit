// JavaScript Document




var timer = 0;
var enemyList = [];
var ypos =-8600;
var score = 0;


var positionMax=2;
var positionMin=0;
var positionPlayer=1;

var start = setInterval(gameStart, 50);

function gameStart() {
	document.onkeydown = checkKey;
    var g = new Game();
	var scoreBoard = document.getElementById('score');
			scoreBoard.innerHTML = score;
			
    if (timer >= 30)
    {
        timer = 0;
        g.createEnemy();
		enemyList.push(g);
		//console.log(enemyList[0].yPosition);
    }
    timer++;
	
	//to detect key press
	function checkKey(e) {
    e = e || window.event;
	var player = document.getElementById("playerCar");

    if (e.keyCode == '39') {
		if(positionPlayer<positionMax){
			positionPlayer++;
		}
    }
    else if (e.keyCode == '37') {
		if(positionPlayer>positionMin){
			positionPlayer--;
		}
    }else if(e.keyCode == '38'){
		g.fire();
		//console.log('fire');
	}
	
	if(positionPlayer==0){
		player.style.left = 15 + 'px';
	}else if(positionPlayer==1){
		player.style.left = 65 + 'px';
	}else if(positionPlayer==2){
		player.style.left = 110 + 'px';
	}
	
	//console.log(positionPlayer);
}


}

function Game() {
    this.xPostion = 0;
    this.yPosition = 0;
	this.xBullet = 0;
	this.yBullet = 0;
	
    this.interverId;
	this.interverIdBullet;
    this.element;
	this.bullet;
	this.enemyPosition=0;
	
    var that = this;

	//create enemy car
    this.createEnemy = function() {
		/*that.track=document.getElementById("track");
		that.trackX = that.trackX + 10;
		that.track.style.top = that.trackX + 'px';*/
        that.element = document.createElement('div');
        that.element.className = 'car';
        document.getElementById('background-wrapper').appendChild(that.element);
        var x = getRandom(3);
		that.enemyPosition=x-1;
        if (x == 1)
            that.xPosition = 15;
        else if (x == 2) {
            that.xPosition = 65;
        } else if (x == 3) {
            that.xPosition = 110;
        }

        that.yPosition = 0;
        that.element.style.top = that.yPosition + "px";
        that.element.style.left = that.xPosition + "px";
        that.intervalId = setInterval(that.moveopponent, 20);
    };
	
	this.fire = function(){
		that.bullet = document.createElement('div');
        that.bullet.className = 'bullet';
        document.getElementById('background-wrapper').appendChild(that.bullet);
		if(positionPlayer==0){
			that.xBullet =15;
		}else if(positionPlayer==1){
			that.xBullet = 65;
		}
		else if(positionPlayer==2){
			that.xBullet = 110;
		}
		that.yBullet= 260;
		that.bullet.style.top = that.yBullet + "px";
        that.bullet.style.left = that.xBullet + "px";
		that.interverIdBullet = setInterval(that.fireStart, 10);
	}
	
	this.fireStart = function(){
		that.yBullet--;
		that.bullet.style.top = that.yBullet + "px";
		//console.log(that.yBullet +","+that.enemyPosition);
		//console.log(enemyList[0].yPosition + ","+that.yBullet);
		for(var i=0; i<enemyList.length; i++){
			if(enemyList[i].xPosition == that.xBullet){
				if(enemyList[i].yPosition == that.yBullet){
					score=score+10;
					console.log("hit"+that.xBullet+","+enemyList[i].xPosition);
					that.bullet.style.display ='none';
					enemyList[i].element.style.display='none';
					
					clearInterval(enemyList[i].intervalId);
					//clearInterval(that.interverIdBullet);
					document.getElementById("background-wrapper").removeChild(enemyList[i].element);
					enemyList.splice(i, 1);
					break;
					
				}
			}
		}
		
		
	}

    this.moveopponent = function () {
		that.yPosition++;
        that.element.style.top = that.yPosition + "px";
		
		if(that.enemyPosition == positionPlayer){
			if(that.yPosition >=230 && that.yPosition<=280){
				if (confirm("Game over!!!\n"+"Your score is: "+score+"\nDo you want to play again")) 
				{
					window.location.href = 'racing_car_start.html';
				} else {
					
				}
				score=0;
			}
		}
        if (that.yPosition >300) {
			var scoreBoard = document.getElementById('score');
			scoreBoard.innerHTML = score;
			
            clearInterval(that.intervalId);
            document.getElementById("background-wrapper").removeChild(that.element);
        }


    }
}

function getRandom(value){
	return(Math.floor((Math.random()*value))+1);
}



